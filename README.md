
Selamat datang di aplikasi tabungan pondok pesantren Nurul Amanah

Untuk teman-teman developer silahkan lakukan langkah-langkah berikut untuk ikut berpartisipasi dalam pembangunan aplikasi Tabungan Nurul Amanah

  

silahkan lakukan perintah berikut untuk meng clone dari repositori kami

    git clone https://gitlab.com/nurul-amanah/e-tabungan-nura.git

Langkah selanjut silahkan jalankan perintah berikut untuk menginstall package yang dibutuhkan oleh project kami

    composer install

Setelah selesai silahkan kalian buat nama file *.env* dan ambil data dari file .env.example jika kamu pengguna Linux maka cukup jalankan perinta berikut

    cp .env.example .env
Setelah itu kalian bisa lakukan perintah migrate untuk membuat database dari file migration yang sudah di sediakan pada project ini

Selamat ngoding teman-teman semoga berkah dan mendapat ilmu yang bermanfaat
