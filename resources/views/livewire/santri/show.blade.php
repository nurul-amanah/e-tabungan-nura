<div>
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Forms</a></li>
                        <li class="breadcrumb-item active">File Uploads</li>
                    </ol>
                </div>
                <h4 class="page-title">Biografi Santri</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-md-4">
            <div class="card-box shadow text-center">
                <div class=text-center>
                    <img src="{{ asset('/assets/images/users/user-1.jpg') }}" class="img-thumbnail" alt="">
                </div>
                <h4 class="font-14 text-upparcase text-center">Abd. Rahman</h4>
                <p class="text-muted">#Santri Pondok Pesantren Nurul Amanah</p>
                <button type="button" class="btn btn-success btn-xs waves-effect mb-2 waves-light">Detail</button>
                <button type="button" class="btn btn-danger btn-xs waves-effect mb-2 waves-light">Prestasi</button>
            </div>
            <div class="card shadow">
                <div class="card-body" dir="ltr">
                    <div class="card-widgets">
                        <a href="javascript: void(0);" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                        <a data-toggle="collapse" href="#cardCollpase2" role="button" aria-expanded="true"
                            aria-controls="cardCollpase2" class=""><i class="mdi mdi-minus"></i></a>
                        <a href="javascript: void(0);" data-toggle="remove"><i class="mdi mdi-close"></i></a>
                    </div>
                    <h4 class="header-title mb-0">Jumlah Tabungan </h4>

                    <div id="cardCollpase2" class="pt-3 collapse show" style="">
                        <div class="text-center">
                            <div class="row mt-2">
                                <div class="col-4">
                                    <h3 data-plugin="counterup">2,845</h3>
                                    <p class="text-muted font-13 mb-0 text-truncate">Total Tabungan</p>
                                </div>
                                <div class="col-4">
                                    <h3 data-plugin="counterup">6,487</h3>
                                    <p class="text-muted font-13 mb-0 text-truncate">Pemasukan</p>
                                </div>
                                <div class="col-4">
                                    <h3 data-plugin="counterup">5,487</h3>
                                    <p class="text-muted font-13 mb-0 text-truncate">Pengeluaran</p>
                                </div>
                            </div> <!-- end row -->

                            <div id="income-amounts" data-colors="#4a81d4,#e3eaef"
                                style="height: 270px; position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"
                                class="morris-chart mt-3"><svg height="270" version="1.1" width="472"
                                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    style="overflow: hidden; position: relative; left: -0.8125px;">
                                    <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël
                                        2.3.0</desc>
                                    <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text
                                        x="33.03125" y="229" text-anchor="end" font-family="sans-serif" font-size="12px"
                                        stroke="none" fill="#888888"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                        font-weight="normal">
                                        <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan>
                                    </text>
                                    <path fill="none" stroke="#41505f" d="M45.53125,229.5H447" stroke-opacity="0.07"
                                        stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                    </path><text x="33.03125" y="178" text-anchor="end" font-family="sans-serif"
                                        font-size="12px" stroke="none" fill="#888888"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                        font-weight="normal">
                                        <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">50</tspan>
                                    </text>
                                    <path fill="none" stroke="#41505f" d="M45.53125,178.5H447" stroke-opacity="0.07"
                                        stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                    </path><text x="33.03125" y="127" text-anchor="end" font-family="sans-serif"
                                        font-size="12px" stroke="none" fill="#888888"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                        font-weight="normal">
                                        <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">100</tspan>
                                    </text>
                                    <path fill="none" stroke="#41505f" d="M45.53125,127.5H447" stroke-opacity="0.07"
                                        stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                    </path><text x="33.03125" y="76" text-anchor="end" font-family="sans-serif"
                                        font-size="12px" stroke="none" fill="#888888"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                        font-weight="normal">
                                        <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">150</tspan>
                                    </text>
                                    <path fill="none" stroke="#41505f" d="M45.53125,76.5H447" stroke-opacity="0.07"
                                        stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                    </path><text x="33.03125" y="25" text-anchor="end" font-family="sans-serif"
                                        font-size="12px" stroke="none" fill="#888888"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                        font-weight="normal">
                                        <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">200</tspan>
                                    </text>
                                    <path fill="none" stroke="#41505f" d="M45.53125,25.5H447" stroke-opacity="0.07"
                                        stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                    </path><text x="447" y="241.5" text-anchor="middle" font-family="sans-serif"
                                        font-size="12px" stroke="none" fill="#888888"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                        font-weight="normal" transform="matrix(1,0,0,1,0,8)">
                                        <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2018
                                        </tspan>
                                    </text><text x="380.1495922673358" y="241.5" text-anchor="middle"
                                        font-family="sans-serif" font-size="12px" stroke="none" fill="#888888"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                        font-weight="normal" transform="matrix(1,0,0,1,0,8)">
                                        <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2017
                                        </tspan>
                                    </text><text x="313.1160327326642" y="241.5" text-anchor="middle"
                                        font-family="sans-serif" font-size="12px" stroke="none" fill="#888888"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                        font-weight="normal" transform="matrix(1,0,0,1,0,8)">
                                        <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2016
                                        </tspan>
                                    </text><text x="246.265625" y="241.5" text-anchor="middle" font-family="sans-serif"
                                        font-size="12px" stroke="none" fill="#888888"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                        font-weight="normal" transform="matrix(1,0,0,1,0,8)">
                                        <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2015
                                        </tspan>
                                    </text><text x="179.41521726733578" y="241.5" text-anchor="middle"
                                        font-family="sans-serif" font-size="12px" stroke="none" fill="#888888"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                        font-weight="normal" transform="matrix(1,0,0,1,0,8)">
                                        <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2014
                                        </tspan>
                                    </text><text x="112.56480953467154" y="241.5" text-anchor="middle"
                                        font-family="sans-serif" font-size="12px" stroke="none" fill="#888888"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                        font-weight="normal" transform="matrix(1,0,0,1,0,8)">
                                        <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013
                                        </tspan>
                                    </text><text x="45.53125" y="241.5" text-anchor="middle" font-family="sans-serif"
                                        font-size="12px" stroke="none" fill="#888888"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                        font-weight="normal" transform="matrix(1,0,0,1,0,8)">
                                        <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2012
                                        </tspan>
                                    </text>
                                    <path fill="#f9fafb" stroke="none"
                                        d="M45.53125,198.4L112.56480953467154,86.19999999999999L179.41521726733578,137.2L246.265625,86.19999999999999L313.1160327326642,137.2L380.1495922673358,86.19999999999999L447,76L447,229L45.53125,229Z"
                                        fill-opacity="1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></path>
                                    <path fill="#90acd6" stroke="none"
                                        d="M45.53125,218.8L112.56480953467154,152.5L179.41521726733578,178L246.265625,152.5L313.1160327326642,178L380.1495922673358,152.5L447,137.2L447,229L45.53125,229Z"
                                        fill-opacity="1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></path>
                                    <path fill="none" stroke="#e3eaef"
                                        d="M45.53125,198.4L112.56480953467154,86.19999999999999L179.41521726733578,137.2L246.265625,86.19999999999999L313.1160327326642,137.2L380.1495922673358,86.19999999999999L447,76"
                                        stroke-width="1" class="line_1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <circle cx="45.53125" cy="198.4" r="3" fill="#ffffff" stroke="#999999"
                                        stroke-width="1" class="circle_line_1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="112.56480953467154" cy="86.19999999999999" r="3" fill="#ffffff"
                                        stroke="#999999" stroke-width="1" class="circle_line_1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="179.41521726733578" cy="137.2" r="3" fill="#ffffff" stroke="#999999"
                                        stroke-width="1" class="circle_line_1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="246.265625" cy="86.19999999999999" r="3" fill="#ffffff" stroke="#999999"
                                        stroke-width="1" class="circle_line_1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="313.1160327326642" cy="137.2" r="3" fill="#ffffff" stroke="#999999"
                                        stroke-width="1" class="circle_line_1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="380.1495922673358" cy="86.19999999999999" r="3" fill="#ffffff"
                                        stroke="#999999" stroke-width="1" class="circle_line_1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="447" cy="76" r="3" fill="#ffffff" stroke="#999999" stroke-width="1"
                                        class="circle_line_1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                    </circle>
                                    <path fill="none" stroke="#4a81d4"
                                        d="M45.53125,218.8L112.56480953467154,152.5L179.41521726733578,178L246.265625,152.5L313.1160327326642,178L380.1495922673358,152.5L447,137.2"
                                        stroke-width="1" class="line_0"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                    <circle cx="45.53125" cy="218.8" r="3" fill="#ffffff" stroke="#999999"
                                        stroke-width="1" class="circle_line_0"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="112.56480953467154" cy="152.5" r="3" fill="#ffffff" stroke="#999999"
                                        stroke-width="1" class="circle_line_0"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="179.41521726733578" cy="178" r="3" fill="#ffffff" stroke="#999999"
                                        stroke-width="1" class="circle_line_0"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="246.265625" cy="152.5" r="3" fill="#ffffff" stroke="#999999"
                                        stroke-width="1" class="circle_line_0"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="313.1160327326642" cy="178" r="3" fill="#ffffff" stroke="#999999"
                                        stroke-width="1" class="circle_line_0"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="380.1495922673358" cy="152.5" r="3" fill="#ffffff" stroke="#999999"
                                        stroke-width="1" class="circle_line_0"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                    <circle cx="447" cy="137.2" r="3" fill="#ffffff" stroke="#999999" stroke-width="1"
                                        class="circle_line_0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                    </circle>
                                </svg>
                                <div class="morris-hover morris-default-style"
                                    style="left: 2px; top: 126px; display: none;">
                                    <div class="morris-hover-row-label">2012</div>
                                    <div class="morris-hover-point" style="color: #e3eaef">
                                        Litecoin:
                                        20
                                    </div>
                                    <div class="morris-hover-point" style="color: #4a81d4">
                                        Bitcoin:
                                        10
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div> <!-- end collapse-->
                </div>
            </div>

        </div>
        <div class="col-md-7">
            <div class="card-box">
                <ul class="nav nav-pills navtab-bg nav-justified">
                    <li class="nav-item">
                        <a href="#informasikewajiban" data-toggle="tab" aria-expanded="false" class="nav-link">
                            Informasi Kewajiban Santri
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#informasikeluarga" data-toggle="tab" aria-expanded="true" class="nav-link">
                            Informasi Keluarga
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#personalinfo" data-toggle="tab" aria-expanded="false" class="nav-link active">
                            Personal Info Santri
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="informasikewajiban">
                        <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-account-circle mr-1"></i>
                            Informasi Kewajiban Santri</h5>
                        <div class="card-box">
                            <div class="table-responsive">
                                <table class="table table-striped mb-0">
                                    <thead>
                                        <tr class="text-center">
                                            <th>No</th>
                                            <th>Jenis Kewajiban</th>
                                            <th>Uraian</th>
                                            <th>Pereode</th>
                                            <th>Nominal</th>
                                            <th>Keterangan</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>SPP Pondok</td>
                                            <td>Pembayaran Pendidkan Infomal</td>
                                            <td>25 Januari 2021</td>
                                            <td>Rp. 25.000</td>
                                            <td>Belum Lunas</td>

                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>SPP Sekolah</td>
                                            <td>Pembayaran Pendidkan Formal</td>
                                            <td>25 Desember 2020</td>
                                            <td>Rp. 25.000</td>
                                            <td>Lunas</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>SPP Diniyah</td>
                                            <td>Pembayaran Pendidkan informal</td>
                                            <td>25 Nopember 2020</td>
                                            <td>Rp. 25.000</td>
                                            <td>Belum Lunas</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">4</th>
                                            <td>Iuran Bulanan</td>
                                            <td>Kebersihan dan Administrasi</td>
                                            <td>25 September 2020</td>
                                            <td>Rp. 25.000</td>
                                            <td>belum Lunas</td>
                                        </tr>
                                        <tr>
                                            <th colspan="4">Total</th>
                                            <th scope="row">Rp.75.000</th>
                                            <th>Belum Lunas</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- end table-responsive-->
                        </div>
                        <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-account-circle mr-1"></i>
                            Informasi Pelanggaran Santri</h5>
                        <div class="card-box">
                            <div class="table-responsive">
                                <table class="table table-striped mb-0">
                                    <thead>
                                        <tr class="text-center">
                                            <th>No</th>
                                            <th>Pelanggaran</th>
                                            <th>Uraian</th>
                                            <th>Pereode</th>
                                            <th>Sanksi</th>
                                            <th>Keterangan</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Shalat Berjamaah</td>
                                            <td>Shalat Asyar</td>
                                            <td>25 Januari 2021</td>
                                            <td>Membaca Surat Yasi 3 Kali</td>
                                            <td>Belum Selesai</td>

                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Kajian Kitab Kuning </td>
                                            <td>Pukul 19:00 WIB</td>
                                            <td>25 Desember 2020</td>
                                            <td>Menghafal Imriti</td>
                                            <td>Belum Selesai</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>SMK Nurul Amanah</td>
                                            <td>Absen Jam Pelajaran SI</td>
                                            <td>25 Nopember 2020</td>
                                            <td>Membersihkan Kamar Mandi</td>
                                            <td>Belum Selesai</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">4</th>
                                            <td>Batas Pondok </td>
                                            <td>Keluar lingkungan pondok tanpa ijin</td>
                                            <td>25 September 2020</td>
                                            <td>Hilang atau di potong rambut</td>
                                            <td>Belum Selesai</td>
                                        </tr>
                                        <tr>
                                            <th colspan="4">Sanki</th>
                                            <th colspan="2">Tidak Dapat Ijin Liburan Pondok</th>

                                        </tr>
                                    </tbody>
                                </table>
                            </div> <!-- end table-responsive-->
                        </div>
                    </div> <!-- end tab-pane -->
                    <!-- end about me section content -->

                    <div class="tab-pane active" id="informasikeluarga">
                        <form>
                            <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-account-circle mr-1"></i>
                                Informasi Ayah Santri</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Nomor Induk Keluarga (NIK)</h5>
                                                <p>1050241708900001</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Nomor Tanda Penduduk (KTP)</h5>
                                                <p>1050241708900001</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Nama Ayah</h5>
                                                <p>Rahman Jailani</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Agama</h5>
                                                <p>Islam</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Pendidikan Terakhir</h5>
                                                <p>SMA/Sederajat</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Pekerjaan</h5>
                                                <p>Wiraswasta</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Tempat dan Tanggal Lahir</h5>
                                                <p>Bangkalan, 12 September 1997</p>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Alamat</h5>
                                                <p>Jl. Raya Tanah Merah No. XA Kelurahan Rong Durin Kecamatan Tanah
                                                    Merah
                                                    Kabupaten Bangkalan</p>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>


                            <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-office-building mr-1"></i>
                                Informasi Ibu Santri</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Nomor Induk Keluarga (NIK)</h5>
                                                <p>1050241708900001</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Nomor Tanda Penduduk (KTP)</h5>
                                                <p>1050241708900001</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Nama Ibu</h5>
                                                <p>Zainab</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Agama</h5>
                                                <p>Islam</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Pendidikan Terakhir</h5>
                                                <p>SMA/Sederajat</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Pekerjaan</h5>
                                                <p>Ibu Rumah tangga</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Tempat dan Tanggal Lahir</h5>
                                                <p>Bangkalan, 12 September 1980</p>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Alamat</h5>
                                                <p>Jl. Raya Tanah Merah No. XA Kelurahan Rong Durin Kecamatan Tanah
                                                    Merah
                                                    Kabupaten Bangkalan</p>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>

                            <!-- end row -->
                            <div class="text-right">
                                <button type="submit" class="btn btn-success waves-effect waves-light mt-2 rounded"><i
                                        class="fas fa-print mr-2"></i>Cetak</button>
                            </div>
                        </form>
                    </div>
                    <!-- end timeline content-->
                    <div class="tab-pane active" id="personalinfo">
                        <form>
                            <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-account-circle mr-1"></i>
                                Personal Info</h5>
                            </h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Nama Lengkap</h5>
                                                <p>Abd. Rahman</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Nama Panggilan</h5>
                                                <p>Rahman</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Nomor Induk Keluarga (NIK)</h5>
                                                <p>1050241708900001</p>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Nomor Induk Siswa Nasional (NISN)</h5>
                                                <p>0051231588</p>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Nama Ibu Kandung</h5>
                                                <p>Zainab</p>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Tempat dan Tanggal Lahir</h5>
                                                <p>Bangkalan, 12 September 1999</p>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Saudara Kandung</h5>
                                                <p>2</p>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Anak Ke-</h5>
                                                <p>2</p>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Alamat</h5>
                                                <p>Jl. Raya Tanah Merah No. XA
                                                    Kelurahan Rong Durin Kecamatan Tanah Merah Kabupaten Bangkalan</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Status Pendidikan</h5>
                                                <p>Formal</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Status Kesiswaan</h5>
                                                <p>Pindahan SMK Nurul Jadid Jombang</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Jurusan</h5>
                                                <p>SMK Nurul Amanah Teknik Komputer dan Jaringan</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div><!-- end col -->
                            </div> <!-- end row -->
                            <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-office-building mr-1"></i>
                                Contact Info</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Nomor Telpon</h5>
                                                <p>0823034789xxxx</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Alamat E-Mail</h5>
                                                <p>rahman123@gmail.com</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1">Kontak Person</h5>
                                                <p>0823034789xxxx</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div> <!-- end row -->
                            <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-earth mr-1"></i> Sosial
                            </h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1"><span><i
                                                            class="fab fa-facebook-square mr-2"></i></span>Facebook</h5>
                                                <p>https://web.facebook.com/rahman</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1"><span><i
                                                            class="fab fa-twitter mr-2"></i></span>Twitter</h5>
                                                <p>rahman1123</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1"><span><i
                                                            class="fab fa-instagram mr-2"></i></span>Instagram</h5>
                                                <p>#rahman123</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1"><span><i
                                                            class="fab fa-linkedin mr-2"></i></span>Linkedin</h5>
                                                <p>#rahman123</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1"><span><i
                                                            class="fab fa-skype mr-2"></i></span>Skype</h5>
                                                <p>#rahman123</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <ul class="list-unstyled ">
                                            <li>
                                                <h5 class="mt-0 mb-1"><span><i
                                                            class="fab fa-github mr-2"></i></span>Github</h5>
                                                <p>#rahman123</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div> <!-- end row -->
                            <div class="text-right">
                                <button type="submit" class="btn btn-success waves-effect waves-light mt-2 rounded"><i
                                        class="fas fa-print mr-2"></i>Cetak</button>
                            </div>
                        </form>
                    </div>
                    <!-- end settings content-->
                </div> <!-- end tab-content -->
            </div>
        </div>
    </div>
</div>
