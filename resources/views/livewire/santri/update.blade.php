<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Santri</a></li>
                        <!--<li class="breadcrumb-item"><a href="javascript: void(0);">Data Santri</a></li>-->
                        <li class="breadcrumb-item active">Edit Santri</li>
                    </ol>
                </div>
                <h4 class="page-title">Edit Santri</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row justify-content-md-center">
        <div class="col-md-12">
            <div class="card shadow-sm">
                <img src="" class="responi" alt="">
                <div class="card-header bg-success text-white">
                    Edit Data Santri
                </div>
                <form action="#" method="post" enctype="multipart/form-data">
                <div class="card-body">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama">Nama Lengkap</label>
                                    <input value="{{ old('nama') }}" type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap">
                                    <div class="text-danger">
                                        {{ $errors->first('nama') }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="panggilan">Nama Panggilan</label>
                                    <input value="{{ old('panggilan') }}" type="text" class="form-control" id="panggilan" name="panggilan" placeholder="Nama Panggilan" required="">
                                    <div class="text-danger">
                                        {{ $errors->first('panggilan') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nisn">NISN</label>
                                    <input value="{{ old('nisn') }}" type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN">
                                    <div class="text-danger">
                                        {{ $errors->first('nisn') }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="npsn">NIK</label>
                                    <input value="{{ old('npsn') }}" type="text" class="form-control" id="npsn" name="npsn" placeholder="NIK">
                                    <div class="text-danger">
                                        {{ $errors->first('npsn') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="jeniskelamin">Jenis Kelamin</label><br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="laki-laki" value="laki-laki" {{ old('jeniskelamin')== 'laki-laki' ? 'selected': '' }} name="jeniskelamin" checked>
                                        <label class="custom-control-label" for="laki-laki">Laki laki</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="perempuan" value="perempuan" {{ old('jeniskelamin')== 'perempuan' ? 'selected' : '' }} name="jeniskelamin">
                                        <label class="custom-control-label" for="perempuan">Perempuan</label>
                                    </div>
                                    <div class="text-danger">
                                        {{ $errors->first('jeniskelamin') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tempatlahir">Tempat Lahir</label>
                                    <input type="text" value="{{ old('tempatlahir') }}" class="form-control" id="tempatlahir" name="tempatlahir" placeholder="Tempat Lahir" required="">
                                    <div class="text-danger">
                                        {{ $errors->first('tempatlahir') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tanggallahir">Tanggal Lahir</label>
                                    <input type="text" class="form-control" data-provide="datepicker" data-date-autoclose="true" value="{{ old('tanggallahir') }}" class="form-control" id="tanggallahir" name="tanggallahir" placeholder="Pilih Tanggal" required="">
                                    <div class="text-danger">
                                        {{ $errors->first('tanggallahir') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="anakke">Anak ke</label>
                                    <input type="text" value="{{ old('anakke') }}" class="form-control" id="anakke" name="anakke" placeholder="Anak Ke" required="">
                                    <div class="text-danger">
                                        {{ $errors->first('anakke') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="saudara">Jumlah Saudara</label>
                                    <input type="text" value="{{ old('saudara') }}" class="form-control" id="saudara" name="saudara" placeholder="Jumlah Saudara" required="">
                                    <div class="text-danger">
                                        {{ $errors->first('saudara') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="npsn">Status dalam Keluarga</label><br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="kandung" value="Anak Kandung" {{ old('statuskeluarga')== 'Anak Kandung' ? 'selected': '' }} name="statuskeluarga" checked>
                                        <label class="custom-control-label" for="kandung">Anak Kandung</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="tiri" value="Anak Tiri" {{ old('statuskeluarga')== 'Anak Tiri' ? 'selected' : '' }} name="statuskeluarga">
                                        <label class="custom-control-label" for="tiri">Anak Tiri</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="angkat" value="Anak Angkat" {{ old('statuskeluarga')== 'Anak Angkat' ? 'selected' : '' }} name="statuskeluarga">
                                        <label class="custom-control-label" for="angkat">Anak Angkat</label>
                                    </div>
                                    <div class="text-danger">
                                        {{ $errors->first('statuskeluarga') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tinggalbersama">Tinggal Bersama</label>
                                    <select class="form-control" id="tinggalbersama" name="tinggalbersama">
                                        <option value="">-- Pilih --</option>
                                        <option value="Orang Tua" {{ old('tinggalbersama') == 'Orang Tua' ? 'selected' : '' }}>Orang Tua</option>
                                        <option value="Kakek/Nenek" {{ old('tinggalbersama') == 'Kakek/Nenek' ? 'selected' : '' }}>Kakek/Nenek</option>
                                        <option value="Paman" {{ old('tinggalbersama') == 'Paman' ? 'selected' : '' }}>Paman</option>
                                    </select>
                                    <div class="text-danger">
                                        {{ $errors->first('tinggalbersama') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat" required="">{{ old('alamat') }}</textarea>
                                    <div class="text-danger">
                                        {{ $errors->first('alamat') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="mondokAtauTidak">Pondok?</label>
                                    <select class="form-control" id="pondok" name="pondok" required>
                                        <option value="">-- Pilih --</option>
                                        <option value="Pondok" {{ old('pondok') == 'Mondok' ? 'selected' : '' }}>Pondok</option>
                                        <option value="Tidak" {{ old('pondok') == 'Tidak' ? 'selected' : '' }}>Tidak</option>
                                    </select>
                                    <div class="text-danger">
                                        {{ $errors->first('pondok') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenjangpendidikan">Masuk Jenjang Pendidikan</label>
                                    <select class="form-control" id="jenjangpendidikan" name="jenjangpendidikan">
                                        <option value="">-- Pilih Jenjang Pendidikan --</option>
                                        <option value="SMP Nurul Amanah" {{ old('jenjangpendidikan') == 'SMP Nurul Amanah' ? 'selected' : '' }}>SMP Nurul Amanah</option>
                                        <option value="MTs Nurul Amanah" {{ old('jenjangpendidikan') == 'MTs Nurul Amanah' ? 'selected' : '' }}>MTs Nurul Amanah</option>
                                        <option value="SMA Nurul Amanah" {{ old('jenjangpendidikan') == 'SMA Nurul Amanah' ? 'selected' : '' }}>SMA Nurul Amanah</option>
                                        <option value="SMK Nurul Amanah" {{ old('jenjangpendidikan') == 'SMK' ? 'selected' : '' }}>SMK Nurul Amanah</option>
                                    </select>
                                    <div class="text-danger">
                                        {{ $errors->first('jenjangpendidikan') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jurusan">Pilih Jurusan</label>
                                    <select class="form-control" id="jurusan" name="Jurusan">
                                        <option value="">-- Pilih Jurusan --</option>
                                    </select>
                                    <small>* hanya untuk jenjang SMK & SMA</small>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="statussiswa">Status Siswa</label><br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="siswabaru" value="Siswa Baru" {{ old('statussiswa')== 'Siswa Baru' ? 'selected': '' }} name="statussiswa" checked>
                                        <label class="custom-control-label" for="siswabaru"> Siswa Baru</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="pindahan" value="Pindahan" {{ old('statussiswa')== 'Pindahan' ? 'selected' : '' }} name="statussiswa">
                                        <label class="custom-control-label" for="pindahan"> Pindahan</label>
                                    </div>
                                    <div class="text-danger">
                                        {{ $errors->first('statussiswa') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="asalsekolah">Asal Sekolah</label>
                                    <input value="{{ old('asalsekolah') }}" type="text" class="form-control" id="asalsekolah" name="asalsekolah" placeholder="Asal Sekolah" required="">
                                    <div class="text-danger">
                                        {{ $errors->first('asalsekolah') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="alamatsekolah">Alamat Sekolah</label>
                                    <textarea type="text" class="form-control" id="alamatsekolah" name="alamatsekolah" placeholder="Alamat Sekolah" required="">{{ old('alamatsekolah') }}</textarea>
                                    <div class="text-danger">
                                        {{ $errors->first('alamatsekolah') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-6">
                                <div class="form-group mb-0">
                                    <label>Foto Santri 3x4</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="foto_diri" id="inputGroupFile04" >
                                            <label class="custom-file-label" for="inputGroupFile04">Masukan Foto</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="rekomendasi">Rekomendasi</label>
                                    <select class="form-control" id="rekomendasi" name="rekomendasi">
                                        <option value="">-- Pilih Rekomendasi --</option>
                                        <option value="Teman/Sahabat" {{ old('rekomendasi') == 'Teman/Sahabat' ? 'selected' : '' }}>Teman/Sahabat/Guru</option>
                                        <option value="Sosial Media" {{ old('rekomendasi') == 'Sosial Media' ? 'selected' : '' }}>Sosial Media</option>
                                        <option value="Pamflet" {{ old('rekomendasi') == 'Pamflet' ? 'selected' : '' }}>Pamflet</option>
                                        <option value="Lainnya" {{ old('rekomendasi') == 'Lainnya' ? 'selected' : '' }}>Lainnya</option>
                                    </select>
                                    <div class="text-danger">
                                        {{ $errors->first('rekomendasi') }}
                                    </div>
                                </div>
                            </div> <!-- end col -->
                            <div class="col-md-12" id="rekomdari">
                                <div class="form-group">
                                    <label for="">Nama Teman/Sahabat/Guru</label>
                                    <input type="text" placeholder="Masukan Nama Teman" class="form-control" name="nama_teman" id="nama_teman">
                                </div>
                            </div>
                        </div> <!-- end row -->
                </div>
                <div class="card-header text-center bg-success text-light">
                    Informasi Orang Tua
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    Informasi Ayah
                                </div>
                                <div class="card-body">
                                        <div class="form-group">
                                            <label for="namaayah">Nama Ayah</label>
                                            <input value="{{ old('namaayah') }}" type="text" class="form-control" id="namaayah" name="namaayah" placeholder="Nama Ayah" required="">
                                            <div class="text-danger">
                                                {{ $errors->first('namaayah') }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="agamaayah">Agama Ayah</label>
                                            <select required class="form-control" id="agamaayah" name="agamaayah">
                                                <option value="">-- Pilih Agama --</option>
                                                <option value="Islam" {{ old('agamaayah') == 'Islam' ? 'selected' : '' }}>Islam</option>
                                                <option value="Kristen" {{ old('agamaayah') == 'Kristen' ? 'selected' : '' }}>Kristen</option>
                                                <option value="Budha" {{ old('agamaayah') == 'Budha' ? 'selected' : '' }}>Budha</option>
                                                <option value="Katolik" {{ old('agamaayah') == 'Katolik' ? 'selected' : '' }}>Katolik</option>
                                                <option value="Konghucu" {{ old('agamaayah') == 'Konghucu' ? 'selected' : '' }}>Konghucu</option>
                                            </select>
                                            <div class="text-danger">
                                                {{ $errors->first('agamaayah') }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="pendidikanayah">Pendidikan Terakhir Ayah</label>
                                            <select class="form-control" id="pendidikanayah" name="pendidikanayah">
                                                <option value="">-- Pilih Pendidikan --</option>
                                                <option value="Tidak Sekolah" {{ old('pendidikanayah') == 'Tidak Sekolah' ? 'selected' : '' }}>Tidak Sekolah</option>
                                                <option value="SD Sederajat" {{ old('pendidikanayah') == 'SD Sederajat' ? 'selected' : '' }}>SD Sederajat</option>
                                                <option value="SMP Sederajat" {{ old('pendidikanayah') == 'SMP Sederajat' ? 'selected' : '' }}>SMP Sederajat</option>
                                                <option value="SMA Sederajat" {{ old('pendidikanayah') == 'SMA Sederajat' ? 'selected' : '' }}>SMA Sederajat</option>
                                                <option value="S1 Sederajat" {{ old('pendidikanayah') == 'S1 Sederajat' ? 'selected' : '' }}> S1 Sederajat</option>
                                            </select>
                                            <div class="text-danger">
                                                {{ $errors->first('pendidikanayah') }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="pekerjaanayah">Pekerjaan Ayah</label>
                                            <select class="form-control" id="pekerjaanayah" name="pekerjaanayah">
                                                <option value="">-- Pilih Pekerjaan --</option>
                                                <option value="Tidak Bekerja" {{ old('pekerjaanayah') == 'Tidak Bekerja' ? 'selected' : '' }}>Tidak Bekerja</option>
                                                <option value="Petani" {{ old('pekerjaanayah') == 'Petani' ? 'selected' : '' }}>Petani</option>
                                                <option value="Wiraswasta" {{ old('pekerjaanayah') == 'Wiraswasta' ? 'selected' : '' }}>Wiraswasta</option>
                                                <option value="PNS" {{ old('pekerjaanayah') == 'PNS' ? 'selected' : '' }}>PNS</option>
                                                <option value="TNI/Polri" {{ old('pekerjaanayah') == 'TNI/Polri' ? 'selected' : '' }}>TNI / Polri</option>
                                                <option value="serabutan" {{ old('pekerjaanayah') == 'Serabutan' ? 'selected' : '' }}>Serabutan</option>
                                            </select>
                                            <div class="text-danger">
                                                {{ $errors->first('pekerjaanayah') }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="alamatayah">Alamat Ayah</label>
                                            <textarea type="text" class="form-control" id="alamatayah" name="alamatayah" placeholder="Alamat Ayah" required="">{{ old('alamatayah') }}</textarea>
                                            <div class="text-danger">
                                                {{ $errors->first('alamatayah') }}
                                            </div>
                                        </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    Informasi Ibu
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="namaibu">Nama Ibu</label>
                                        <input type="text" class="form-control" id="namaibu" name="namaibu" placeholder="Nama Ibu" value="{{ old('namaibu')}}" required="">
                                        <div class="text-danger">
                                            {{ $errors->first('namaibu') }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="agamaibu">Agama Ibu</label>
                                        <select class="form-control" id="agamaibu" name="agamaibu">
                                            <option value="">-- Pilih Agama --</option>
                                            <option value="Islam" {{ old('agamaibu') == 'Islam' ? 'selected' : '' }}>Islam</option>
                                            <option value="Kristen" {{ old('agamaibu') == 'Kristen' ? 'selected' : '' }}>Kristen</option>
                                            <option value="Budha" {{ old('agamaibu') == 'Budha' ? 'selected' : '' }}>Budha</option>
                                            <option value="Katolik" {{ old('agamaibu') == 'Katolik' ? 'selected' : '' }}>Katolik</option>
                                            <option value="Konghucu" {{ old('agamaibu') == 'Konghucu' ? 'selected' : '' }}>Konghucu</option>
                                        </select>
                                        <div class="text-danger">
                                            {{ $errors->first('agamaibu') }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="pendidikanibu">Pendidikan Terakhir Ibu</label>
                                        <select class="form-control" id="pendidikanibu" name="pendidikanibu">
                                            <option value="">-- Pilih Pendidikan --</option>
                                            <option value="Tidak Sekolah" {{ old('pendidikanibu') == 'Tidak Sekolah' ? 'selected' : '' }}>Tidak Sekolah</option>
                                            <option value="SD Sederajat" {{ old('pendidikanibu') == 'SD Sederajat' ? 'selected' : '' }}>SD Sederajat</option>
                                            <option value="SMP Sederajat" {{ old('pendidikanibu') == 'SMP Sederajat' ? 'selected' : '' }}>SMP Sederajat</option>
                                            <option value="SMA Sederajat" {{ old('pendidikanibu') == 'SMA Sederajat' ? 'selected' : '' }}>SMA Sederajat</option>
                                            <option value="S1 Sederajat" {{ old('pendidikanibu') == 'S1 Sederajat' ? 'selected' : '' }}> S1 Sederajat</option>
                                        </select>
                                        <div class="text-danger">
                                            {{ $errors->first('pendidikanibu') }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="pekerjaanibu">Pekerjaan Ibu</label>
                                        <select class="form-control" id="pekerjaanibu" name="pekerjaanibu">
                                            <option value="">-- Pilih Pekerjaan --</option>
                                            <option value="Ibu Rumah Tangga" {{ old('pekerjaanibu') == 'Ibu Rumah Tangga' ? 'selected' : '' }}>Ibu Rumah Tangga</option>
                                            <option value="Petani" {{ old('pekerjaanibu') == 'Petani' ? 'selected' : '' }}>Petani</option>
                                            <option value="Wiraswasta" {{ old('pekerjaanibu') == 'Wiraswasta' ? 'selected' : '' }}>Wiraswasta</option>
                                            <option value="PNS" {{ old('pekerjaanibu') == 'PNS' ? 'selected' : '' }}>PNS</option>
                                            <option value="TNI/Polri" {{ old('pekerjaanibu') == 'TNI/Polri' ? 'selected' : '' }}>TNI / Polri</option>
                                            <option value="serabutan" {{ old('pekerjaanibu') == 'Serabutan' ? 'selected' : '' }}>Serabutan</option>
                                        </select>
                                        <div class="text-danger">
                                            {{ $errors->first('pekerjaanibu') }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="alamatibu">Alamat Ibu</label>
                                        <textarea  type="text" class="form-control" id="alamatibu" name="alamatibu" placeholder="Alamat Ibu" required="">{{ old('alamatibu') }}</textarea>
                                        @if ($errors->first('alamatibu'))
                                            <div class="alert alert-danger mt-1" role="alert">
                                                <i class="mdi mdi-block-helper mr-2"></i> {{ $error }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div><!-- end col -->
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="penghasilan">Penghasilan Orang Tua Perbulan</label>
                                        <select class="form-control" id="penghasilan" name="penghasilan">
                                            <option value="">-- Pilih penghasilan --</option>
                                            <option value="0 - 500.000" {{ old('penghasilan') == '0 - 500.000' ? 'selected' : '' }}>0 - 500.000</option>
                                            <option value="500.000 - 1.000.000" {{ old('penghasilan') == '500.000 - 1.000.000' ? 'selected' : '' }}>500.000 - 1.000.000</option>
                                            <option value="1.000.000 - 2.000.000" {{ old('penghasilan') == '1.000.000 - 2.000.000' ? 'selected' : '' }}>1.000.000 - 2.000.000</option>
                                            <option value="2.000.000 - 5.000.000" {{ old('penghasilan') == '2.000.000 - 5.000.000' ? 'selected' : '' }}>2.000.000 - 5.000.000</option>
                                            <option value="5.000.000 - 10.000.000" {{ old('penghasilan') == '5.000.000 - 10.000.000' ? 'selected' : '' }}>5.000.000 - 10.000.000</option>
                                            <option value="10.000.000 Ke atas" {{ old('penghasilan') == '10.000.000 Ke atas' ? 'selected' : '10.000.000 Ke atas' }}>10.000.000 Ke atas</option>
                                        </select>
                                        <div class="text-danger">
                                            {{ $errors->first('penghasilan') }}
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label for="nowali">Nomor HP/WA Orang Tua/Wali</label>
                                        <input type="text" class="form-control" id="nowali" name="nowali" value="{{ old('nowali') }}" placeholder="Nomor HP/WA Orang Tua/Wali" required="">
                                        <div class="text-danger">
                                            {{ $errors->first('nowali') }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-edit"></i> Simpan</button>
                                        <button type="reset" class="btn btn-danger"><i class="fa fa-undo"></i> Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    $(document).ready(function(){
     $('#jenjangpendidikan').change(function () {
         if ($(this).val() == "SMK Nurul Amanah") {
             $('#jurusan').removeAttr('disabled');
             $('#jurusan').children('option:not(:first)').remove();
             $('#jurusan').append(
                 '<option value="Teknik Komputer dan Jaringan">TKJ</option>',
                 '<option value="Teknik Sepeda Motor">TSM</option>',
                 '<option value="Akuntansi">AK</option>',
             );
         }else if($(this).val() == "SMA Nurul Amanah") {
             $('#jurusan').removeAttr('disabled');
             $('#jurusan').children('option:not(:first)').remove();
             $('#jurusan').append(
                 '<option value="Ilmu Pengetahuan Sosial">IPS</option>',
                 '<option value="Ilmu Pengetahuan Alam">IPA</option>',
             );
         }else{
             $('#jurusan').attr('disabled', 'true');
         }
     })


     $('#pondok').change(function(){
         if ($(this).val() === 'Pondok') {
             $('#asrama').removeAttr('disabled');
             $('#asrama').children('option:not(:first)').remove();
             $('#asrama').append(
                 '<option value="Asrama Induk">Asrama Induk</option>',
                 '<option value="Asrama Bahasa">Asrama Bahasa</option>',
             );
         }else{
             $('#asrama').attr('disabled', 'true');
         }
     })

     if ($('#rekomendasi').val() == "") {
         $('#rekomdari').attr('hidden', true)
     }

     $('#rekomendasi').change(function(){
         if ($(this).val() == 'Teman/Sahabat') {
             $('#rekomdari').removeAttr('hidden')
         }else{
             $('#rekomdari').attr('hidden', true)
         }
     })
    })
</script>
@endpush
