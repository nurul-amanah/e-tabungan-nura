<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Tabungan</a></li>
                        <li class="breadcrumb-item active">Nasabah</li>
                    </ol>
                </div>
                <h4 class="page-title">Nasabah</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-md-6 col-xl-6">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-primary">
                            <i class="fe-tag font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">100</span></h3>
                            <p class="text-muted mb-1 text-truncate">Jumlah Nasabah</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-6">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-success">
                            <i class="fe-check-circle font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">10,000,000</span></h3>
                            <p class="text-muted mb-1 text-truncate">Saldo</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="header-title mb-4">Data Nasabah</h4>

                <table class="table table-hover m-0 table-centered dt-responsive nowrap w-100" id="tickets-table">
                    <thead>
                    <tr>
                        <th>NO</th>
                        <th>Rekening</th>
                        <th>Nama Nasabah</th>
                        <th>Alamat</th>
                        <th>Saldo</th>
                        <th>Tanggungan</th>
                        <th>Setor Tunai</th>
                        <th>Tarik Tunai</th>
                        <th class="hidden-sm">Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td><b>1</b></td>
                        <td>208487637868</td>
                        <td>Muhammad Budi</td>
                        <td>Bangkalan</td>
                        <td>500,000</td>
                        <td>300,000</td>
                        <td>
                            <button type="button" class="btn btn-sm btn-blue waves-effect waves-light">
                                <i class="mdi mdi-plus-circle"></i> Setor
                            </button>
                        </td>
                        <td>
                            <button type="button" class="btn btn-sm btn-danger waves-effect waves-light">
                                <i class="mdi mdi-minus-circle"></i> Tarik
                            </button>
                        </td>
                        <td>
                            <div class="btn-group dropdown">
                                <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-pencil mr-2 text-muted font-18 vertical-middle"></i>Edit Nasabah</a>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>Hapus Nasabah</a>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-account mr-2 font-18 text-muted vertical-middle"></i>Profil</a>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-check-all mr-2 text-muted font-18 vertical-middle"></i>Close</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- Care about people's approval and you will be their prisoner. --}}
</div>

