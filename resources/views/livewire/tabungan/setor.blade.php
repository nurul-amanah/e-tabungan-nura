<div class="container-fluid">
    {{-- Nothing in the world is as soft and yielding as water. --}}

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Tabungan</a></li>
                        <li class="breadcrumb-item active">Setor Tunai</li>
                    </ol>
                </div>
                <h4 class="page-title">Setor Tunai</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="row ">
        <div class="col-md-12">
            <div class="card shadow-sm">
                <img src="" class="responi" alt="">
                <div class="card-header bg-blue text-white">
                    Setor Tunai
                </div>
                <div class="card-body">
                    <div class="row justify-content-md-center">
                        <div class="col-md-5">
                            <form action="#" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="rek">No. Rekening</label>
                                    <input value="{{ old('rek') }}" type="text" class="form-control" id="rek" name="rek" placeholder="No. Rekening ">
                                    <div class="text-danger">
                                        {{ $errors->first('rek') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama">Nama Nasabah</label>
                                    <input value="{{ old('nama') }}" type="text" class="form-control" id="nama" name="nama" placeholder="Nama Nasabah">
                                    <div class="text-danger">
                                        {{ $errors->first('nama') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nominal_setor">Nominal</label>
                                    <input value="{{ old('nominal_setor') }}" type="number" class="form-control" id="nominal_setor" name="nominal_setor" placeholder="Nominal">
                                    <div class="text-danger">
                                        {{ $errors->first('nominal_setor') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input value="{{ old('password') }}" type="password" class="form-control" id="password" name="password" placeholder="Password">
                                    <div class="text-danger">
                                        {{ $errors->first('nominal_setor') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <a href="#" class="btn btn-info" data-animation="fadein" data-plugin="custommodal">Setor</a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
