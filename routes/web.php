<?php

use App\Http\Livewire\Santri\Create;
use App\Http\Livewire\Santri\Index;
use App\Http\Livewire\Santri\Show;
use App\Http\Livewire\Santri\Update;
use App\Http\Livewire\Tabungan\Index as TabunganIndex;
use App\Http\Livewire\Tabungan\Setor;
use App\Http\Livewire\Tabungan\Tarik;
use App\Http\Livewire\Tabungan\Update as TabunganUpdate;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('santri.index');
});

Route::group(['prefix' => 'admin'], function () {
    Route::group(['prefix' => 'santri'], function () {
        Route::get('semua', Index::class)->name('santri.index');
        Route::get('tambah', Create::class)->name('santri.create');
        Route::get('{slug}/profile', Show::class)->name('santri.show');
        Route::get('{slug}/sunting', Update::class)->name('santri.update');
    });
    Route::group(['prefix' => 'tabungan'], function () {
        Route::get('semua', TabunganIndex::class)->name('tabungan.index');
        Route::get('tambah', Setor::class)->name('tabungan.setor');
        Route::get('tarik', Tarik::class)->name('tabungan.tarik');
        Route::get('ubah-setoran{/id}', TabunganUpdate::class)->name('tabungan.update');
    });
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
