<?php

namespace App\Http\Livewire\Santri;

use Livewire\Component;

class Show extends Component
{
    public function render()
    {
        return view('livewire.santri.show');
    }
}
