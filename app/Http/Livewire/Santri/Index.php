<?php

namespace App\Http\Livewire\Santri;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.santri.index');
    }
}
