<?php

namespace App\Http\Livewire\Santri;

use Livewire\Component;

class Create extends Component
{
    public function render()
    {
        return view('livewire.santri.create');
    }
}
