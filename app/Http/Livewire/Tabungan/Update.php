<?php

namespace App\Http\Livewire\Tabungan;

use Livewire\Component;

class Update extends Component
{
    public function render()
    {
        return view('livewire.tabungan.update');
    }
}
