<?php

namespace App\Http\Livewire\Tabungan;

use Livewire\Component;

class Setor extends Component
{
    public function render()
    {
        return view('livewire.tabungan.setor');
    }
}
