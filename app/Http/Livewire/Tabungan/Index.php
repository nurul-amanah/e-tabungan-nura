<?php

namespace App\Http\Livewire\Tabungan;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.tabungan.index');
    }
}
